
				/******************************************************************************/
				//Area data graph
				/******************************************************************************/

					function GetAreaData(UserNumber)
					{
						if (AreaGData[UserNumber].length > 0)
							AreaGData[UserNumber] = AreaGData[UserNumber].slice(1);

						// Do a random walk

						while (AreaGData[UserNumber].length < AreaTotalPoints)
						{
							if (AreaData[UserNumber].length > AreaPlotCounter)
							{
									AreaGData[UserNumber].push(AreaData[UserNumber][AreaPlotCounter]);
									//console.log(XAxisData[XAxisPlotCounter]);
									AreaPlotCounter++;
							}
							else
								AreaGData[UserNumber].push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < AreaGData[UserNumber].length; ++i) {
							res.push([i, AreaGData[UserNumber][i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					AreaSeriesObj = function() 
					{
						 return [
							{
								data: GetAreaData(0),
								lines: { show: true, fill: false },
								color: User1Color
							}, 
							{
								data: GetAreaData(1),
								lines: { show: true, fill: false },
								color: User2Color
							},
						];
					}
					var AreaOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -3,
							max: 5
						},
						xaxis: {
							show: false
						},					
						yaxis: {
							min: -0.1,
							max: 0.4
						}					
					}

					/******************************************************************************/
					var AreaPlot = $.plot("#AreaPlot", AreaSeriesObj(), AreaOptions);
					/******************************************************************************/
					function UpdateAreaPlot() 
					{
						AreaPlot.setData(AreaSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						AreaPlot.draw();
						setTimeout(UpdateAreaPlot, updateInterval);
					}
					UpdateAreaPlot();
					/******************************************************************************/
					/******************************************************************************/

