				/******************************************************************************/
				//Y-Axis data graph
				/******************************************************************************/

					function GetYAxisData(UserNumber)
					{
						if (YAxisGData[UserNumber].length > 0)	YAxisGData[UserNumber] = YAxisGData[UserNumber].slice(1);
						while (YAxisGData[UserNumber].length < YAxisTotalPoints)
						{
							if (YAxisData[UserNumber].length > YAxisPlotCounter)
							{
									YAxisGData[UserNumber].push(YAxisData[UserNumber][YAxisPlotCounter]);
									YAxisPlotCounter++;
							}
							else YAxisGData[UserNumber].push(0);
						}
						var res = [];
						for (var i = 0; i < YAxisGData[UserNumber].length; ++i) {
							res.push([i, YAxisGData[UserNumber][i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					YAxisSeriesObj = function() 
					{
						 return [
							{
								data: GetYAxisData(0),
								lines: { show: true, fill: false },
								color: User1Color
							}, 
							{
								data: GetYAxisData(1),
								lines: { show: true, fill: false },
								color: User1Color
							},
						];
					}
					var YAxisOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -3,
							max: 5
						},
						yaxis: {
							min: 0,
							max: 1920
						},					
					}

					/******************************************************************************/				
					/******************************************************************************/

		 			var YAxisPlot = $.plot("#YAxisPlot", YAxisSeriesObj(), YAxisOptions);
					/******************************************************************************/
					function UpdateYAxisPlot() 
					{
						YAxisPlot.setData( YAxisSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						YAxisPlot.draw();
						setTimeout(UpdateYAxisPlot, updateInterval);
					}
					UpdateYAxisPlot();
					/******************************************************************************/
					/******************************************************************************/

				/******************************************************************************/
				//XY-Axis data graph
				/******************************************************************************/

					function GetXAxisData(UserNumber)
					{
						if (XAxisGData[UserNumber].length > 0)	XAxisGData[UserNumber] = XAxisGData[UserNumber].slice(1);
						while (XAxisGData[UserNumber].length < XAxisTotalPoints)
						{
							if (XAxisData[UserNumber].length > XAxisPlotCounter)
							{
									XAxisGData[UserNumber].push(XAxisData[UserNumber][XAxisPlotCounter]);
									XAxisPlotCounter++;
							}
							else XAxisGData[UserNumber].push(0);
						}
						var res = [];
						for (var i = 0; i < XAxisGData[UserNumber].length; ++i) {
							res.push([i, XAxisGData[UserNumber][i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					XAxisSeriesObj = function() 
					{
						 return [
							{
								data: GetXAxisData(0),
								lines: { show: true, fill: false },
								color: User1Color
							}, 
							{
								data: GetXAxisData(1),
								lines: { show: true, fill: false },
								color: User2Color
							},
						];
					}
					var XAxisOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -3,
							max: 5
						},
						yaxis: {
							min: 0,
							max: 1080
						},					
					}

					/******************************************************************************/				
					/******************************************************************************/

		 			var XAxisPlot = $.plot("#XAxisPlot", XAxisSeriesObj(), XAxisOptions);
					/******************************************************************************/
					function UpdateXAxisPlot() 
					{
						XAxisPlot.setData( XAxisSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						XAxisPlot.draw();
						setTimeout(UpdateXAxisPlot, updateInterval);
					}
					UpdateXAxisPlot();
					/******************************************************************************/
					/******************************************************************************/




