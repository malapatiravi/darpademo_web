
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Multi Bar Chart with Values at Top (Flot) - jsFiddle demo</title>
 		<script type="text/javascript" src="../jquery-1.11.0.min.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.orderBars.js"></script>
</head>
<body style="text-align:center">
	<hr/><h2>Swipe Pattern 1 Average</h2><br/>
  <div id="placeholder1" style="width:700px;height:400px;text-align:center;margin:auto"></div>
	<hr/><h2>Swipe Pattern 2 Average</h2><br/>
  <div id="placeholder2" style="width:700px;height:400px;text-align:center;margin:auto"></div>
	<hr/><h2>Swipe Pattern 3 Averag</h2>e<br/>
  <div id="placeholder3" style="width:700px;height:400px;text-align:center;margin:auto"></div>
	<hr/><h2>Swipe Pattern 4 Average</h2><br/>
  <div id="placeholder4" style="width:700px;height:400px;text-align:center;margin:auto"></div>
	<hr/><h2>Swipe Pattern 5 Average</h2><br/>
  <div id="placeholder5" style="width:700px;height:400px;text-align:center;margin:auto"></div>


<script type='text/javascript'>//<![CDATA[ 

$(function () {
				var a1 = [[0,0.8675925926],[	1,0.8097916667],[	2,0.7646296296],[	3,0.17821605],[	4,1],[	5,0.5579108413]];
				var a3 = [[0,0.6261111111],[	1,0.5278125],[	2,0.6376156111],[	3,0.3392848854],[	4,0.7898114721],[	5,0.1644084195]];
				var a5 = [[0,0.8244444444],[	1,0.4458333333],[	2,0.914031037],[	3,0.2174306719],[	4,0.8042519053],[	5,0.1994980911]];
				var a7 = [[0,0.5822222222],[	1,0.6863541667],[	2,0.4085188333],[	3,0.2820805521],[	4,0.9229843562],[	5,0.3624935542]];
				var a9 = [[0,0.5685185185],[	1,0.6446875],[	2,0.6523313815],[	3,0.2509985833],[	4,0.644003209],[	5,0.3461215594]];
					
					
					
				var a2 = [[0,0.8581934037],[	1,0.8195976667],[	2,0.7766753741],[	3,0.1512186458],[	4,0.9048335339],[	5,0.5838874863]];
				var a4 = [[0,0.6360100222],[	1,0.5373182708],[	2,0.6477483352],[	3,0.3538566146],[	4,0.7466907341],[	5,0.1599974744]];
				var a6 = [[0,0.8057851852],[	1,0.445994043	],[2,0.8848504653],[	3,0.2129459792],[	4,0.7783794625],[	5,0.2067758776]];
				var a8 = [[0,0.58268675],[	1,0.6936492708],[	2,0.4111819389],[	3,0.2807606115],[	4,0.9310068191],[	5,0.3695404587]];
				var a10 = [[0,0.587859587],[	1,0.6268354688],[	2,0.6452521519],[	3,0.2482803844],[	4,0.6019855596],[	5,0.3311230058]];

				/*
				var a1 = [[0,0.8675925926],[1,0.8097916667],[	2,0.7646296296],[	3,0.17821605],[	4,1	5,0.5579108413]];
				var a2 = [[0,0.6261111111],[	1,0.5278125],[	2,0.6376156111],[	3,0.3392848854],[	4,0.7898114721],[	5,0.1644084195]];
				var a3 = [[0,0.8244444444],[	1,0.4458333333],[	2,0.914031037],[	3,0.2174306719],[	4,0.8042519053],[	5,0.1994980911]];
				var a4 = [[0,0.5822222222],[	1,0.6863541667],[	2,0.4085188333],[	3,0.2820805521],[	4,0.9229843562],[	5,0.3624935542]];
				var a5 = [[0,0.5685185185],[	1,0.6446875],[	2,0.6523313815],[	3,0.2509985833],[	4,0.644003209],[	5,0.3461215594]];
					
					
					
				var a6 = [[0,0.8581934037],[	1,0.8195976667],[	2,0.7766753741],[	3,0.1512186458],[	4,0.9048335339],[	5,0.5838874863]];
				var a7 = [[0,0.6360100222],[	1,0.5373182708],[	2,0.6477483352],[	3,0.3538566146],[	4,0.7466907341],[	5,0.1599974744]];
				var a8 = [[0,0.8057851852],[	1,0.445994043	],[2,0.8848504653],[	3,0.2129459792],[	4,0.7783794625],[	5,0.2067758776]];
				var a9 = [[0,0.58268675],[	1,0.6936492708],[	2,0.4111819389],[	3,0.2807606115],[	4,0.9310068191],[	5,0.3695404587]];
				var a10 = [[0,0.587859587],[	1,0.6268354688],[	2,0.6452521519],[	3,0.2482803844],[	4,0.6019855596],[	5,0.3311230058]];
				*/
        var data1 = [
            {
                label: "NAO",
                data: a1,
                color: "green"
            },
            {
                label: "User",
                data: a2,
                color:"black"
            }
        ];
        var data2 = [
            {
                label: "NAO",
                data: a3,
                color: "green"
            },
            {
                label: "User",
                data: a4,
                color:"black"
            }
        ];
        var data3 = [
            {
                label: "NAO",
                data: a5,
                color: "green"
            },
            {
                label: "User",
                data: a6,
                color:"black"
            }
        ];
        var data4 = [
            {
                label: "NAO",
                data: a7,
                color: "green"
            },
            {
                label: "User",
                data: a8,
                color:"black"
            }
        ];
        var data5 = [
            {
                label: "NAO",
                data: a9,
                color: "green"
            },
            {
                label: "User",
                data: a10,
                color:"black"
            }
        ];

        $.plot($("#placeholder1"), data1, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.13,
                    order: 1
                }
            },
            valueLabels: {
                show: true
            },
						xaxis: { ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time'],[5,'Angle']]}  ,
						yaxis: {max:1, min:0}
        });
        $.plot($("#placeholder2"), data2, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.13,
                    order: 1
                }
            },
            valueLabels: {
                show: true
            },
						xaxis: { ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time'],[5,'Angle']]}  ,
						yaxis: {max:1, min:0}
        });
        $.plot($("#placeholder3"), data3, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.13,
                    order: 1
                }
            },
            valueLabels: {
                show: true
            },
						xaxis: { ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time'],[5,'Angle']]}  ,
						yaxis: {max:1, min:0}
        });
        $.plot($("#placeholder4"), data4, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.13,
                    order: 1
                }
            },
            valueLabels: {
                show: true
            },
						xaxis: { ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time'],[5,'Angle']]}  ,
						yaxis: {max:1, min:0}
        });
        $.plot($("#placeholder5"), data5, {
            series: {
                bars: {
                    show: true,
                    barWidth: 0.13,
                    order: 1
                }
            },
            valueLabels: {
                show: true
            },
						xaxis: { ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time'],[5,'Angle']]}  ,
						yaxis: {max:1, min:0}
        });

    });
//]]>  

</script>


</body>


</html>


