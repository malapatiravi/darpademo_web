<? 
	$TotalSwipes = 3;
	$CurrentSwipe = isset($_GET['SwipeNo'])?$_GET['SwipeNo']:"1";
	//$CurrentUser = "Suzzett"; if (isset($_GET['CurrentUser'])) $CurrentUser = $_GET['CurrentUser']
?>
<!DOCTYPE html>
<html>
 	<head>
 		<title>Active Authentication </title>
 		<script type="text/javascript" src="../jquery-1.11.0.min.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.js"></script>
		<script language="JavaScript" type="text/javascript" src="../flot/curvedLines.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.orderBars.js"></script>
		<link href="stylev2.css" rel="stylesheet" type="text/css">
 	</head>
	<body>
		
		<div id="CompleteContainer">
			<div id="Header">
				<div id="NAOImage"><img src="../nao.png"/></div>
				<div id="HeaderTitle"><h1>Robotic attack by NAO</h1></div>
			</div>
			<div><hr/></div>
				<div class="SwipesList" id="SwipeListDiv">
					<label>Select Swipe:</label>
						<select id="SwipeMenu" class="SwipeSelection">
							<?
								for ($i = 1; $i <= $TotalSwipes; $i++)
								{
									$OptionVal = "Swipe ".$i;
							?>
									<option <?=($CurrentSwipe==$i)?"selected":""?> value="<?=$i?>"><?=$OptionVal?></option>
							<?	
								}
							?>
						</select>
						<!--<span id="InitializeButtonSpan"><input type="button" id="StartData" class="CustomButton" value="Initialize"/></span>				
						<span id="InitializeButtonSpan"><input type="button" id="StartData" class="CustomButton" value="Clear Data"/></span>
						-->
				</div>
			<div id="MainBody">				
				<div id="SwipeComparision">
					<h2><u>Swipe Comparision</u></h2><br/>
					<div id="OriginalSwipe">
						<div id="OriginalSwipePlaceholder" class="demo-placeholder"></div>
						Original Swipe
					</div>
					<div id="ForgedSwipe">
						<div id="ForgedSwipePlaceholder" class="demo-placeholder"></div>
						Forged Swipe
					</div>
				</div>
				<div class="clear"><hr/></div>
				<div id="GraphOfComparision"><h2><u>Features Comparision</u></h2>
					<div id="SwipeComparision">
						<div id="ComparisionPlaceholder" class="demo-placeholder"></div>
							Comparision placeholder
					</div>
				</div>
			</div>
			<script type="text/javascript">
				$('#SwipeMenu').on('change', function()
				{
					CurrentSelectedOption = $('#SwipeMenu').find(":selected").text().replace(/\s/g, '').replace("Swipe", "");
					location.href = "?SwipeNo="+CurrentSelectedOption;
				});
				
				var ComparisionPlot;// = $.plot("#ComparisionPlaceholder", ClassifierSeriesObj(), ComparisionOptions);
				var OrigSwipePlot;// = $.plot("#OriginalSwipePlaceholder", [ OrigPlotData], SwipesOptions);
				var ForgedPlot;
				var IndexOfUserID = 0;
				var IndexOfAction = 1;
				var IndexOfOrientation = 2;
				var IndexOfX = 3;
				var IndexOfY = 4;
				var IndexOfPressure = 5;
				var IndexOfSize = 6;
				var IndexOfTime = 7;


				var Pause = false;
				var RealXVals = Array();
				var RealYVals = Array();
				var RealTime = Array();

				var RealInitX = 0;
				var RealInitY = 0;
				var RealEndX = 0;
				var RealEndY = 0;
				var RealTimeDiff = 0;
				var RealAngle = 0;

				var RobotXVals = Array();
				var RobotYVals = Array();
				var RobotTime = Array();

				var RobotInitX = 0;
				var RobotInitY = 0;
				var RobotEndX = 0;
				var RobotEndY = 0;
				var RobotTimeDiff = 0;
				var RobotAngle = 0;


				var OrigPlotData = Array();
				var RobotPlotData = Array();
				var ComparisionRobot = Array();
				var ComparisionOrig = Array();
				
				SwipeNumber = <?=$CurrentSwipe?>;
				
				
				
				function GetDataFromServer(RobotOrReal, SwipeNum)
				{
					if (RobotOrReal == "Real")
					{
						$.ajax({
							type: 'POST',
							url: "GetAllData.php",
								data: {RequestFor: RobotOrReal, SwipeNum: SwipeNumber},							
								success: function(ServerData)
								{
									var AllData = ServerData.Data.split(":-:");
									for (index = 0; index < AllData.length; ++index)
									{
										var IndvData = AllData[index].split(",");
										if (IndvData[IndexOfX] === undefined)
										{
											//console.log("Is empty");
										}
										else
										{
											RealXVals.push(IndvData[IndexOfX]);
											RealYVals.push(IndvData[IndexOfY]);
											RealTime.push(IndvData[IndexOfTime]);
											//console.log(AllData[index]);
											//console.log(IndvData[IndexOfX]);
										}

									}
									//console.log(AllData);
									for (var index = 0; index < RealXVals.length; index++)
									{
										OrigPlotData.push([RealYVals[index], RealXVals[index]]);
									}
									///console.log(RealXVals.toString());
									RealInitX = RealXVals[0]/1080;
									RealInitY = RealYVals[0]/1920;
									RealEndX = RealXVals[RealXVals.length-1]/1080;
									RealEndY = RealYVals[RealYVals.length-1]/1920;
									RealTimeDiff = RealTime[RealTime.length-1]-RealTime[0];
									
									var XDiff = RealEndX - RealInitX;
									var YDiff = RealEndY - RealInitY;
									RealAngle = Math.atan(YDiff/XDiff);
									//console.log(RealInitX+"=RealInitX,  "+RealInitY+"=RealInitY,  "+RealEndX+"=RealEndX,  "+RealEndY+"=RealEndY,  "+RealTimeDiff+"=RealTime,  "+RealAngle+"=RealAngle");
								},
								error : function(jqXHR, textStatus, errorThrown)
								{
									console.log("There was an error: "+errorThrown);
								},
								dataType: "json"
						});
					}
					else
					{
						$.ajax({
							type: 'POST',
							url: "GetAllData.php",
								data: {RequestFor: RobotOrReal, SwipeNum: SwipeNumber},							
								success: function(ServerData)
								{
									var AllData = ServerData.Data.split(":-:");
									for (index = 0; index < AllData.length; ++index)
									{
										var IndvData = AllData[index].split(",");
										if (IndvData[IndexOfX] === undefined)
										{
											//console.log("Is empty");
										}
										else
										{
											RobotXVals.push(IndvData[IndexOfX]);
											RobotYVals.push(IndvData[IndexOfY]);
											RobotTime.push(IndvData[IndexOfTime]);
											//console.log(AllData[index]);
										}
									}
									for (var index = 0; index < RobotXVals.length; index++)
									{
										RobotPlotData.push([RobotYVals[index], RobotXVals[index]]);
									}
									///console.log(RealXVals.toString());
									RobotInitX = RobotXVals[0]/1080;
									RobotInitY = RobotYVals[0]/1920;
									RobotEndX = RobotXVals[RobotXVals.length-1]/1080;
									RobotEndY = RobotYVals[RobotYVals.length-1]/1920;
									RobotTimeDiff = RobotTime[RobotTime.length-1]-RobotTime[0];
									
									var XDiff = RobotEndX - RobotInitX;
									var YDiff = RobotEndY - RobotInitY;
									var RobotAngle = Math.atan(YDiff/XDiff);
									
									console.log(RobotInitX+"=RobotInitX,  "+RobotInitY+"=RobotInitY,  "+RobotEndX+"=RobotEndX,  "+RobotEndY+"=RobotEndY,  "+RobotTimeDiff+"=RobotTime,  "+RobotAngle+"=RobotAngle");
									console.log(RealInitX+"=RealInitX,  "+RealInitY+"=RealInitY,  "+RealEndX+"=RealEndX,  "+RealEndY+"=RealEndY,  "+RealTimeDiff+"=RealTime,  "+RealAngle+"=RealAngle");

									if (RealAngle>RobotAngle)
									{
										RobotAngle = RobotAngle/RealAngle;
										RealAngle = 1;
									}
									else
									{
										RealAngle = RealAngle/RobotAngle;
										RobotAngle = 1;									
									}
									if (RealTimeDiff>RobotTimeDiff)
									{
										RobotTimeDiff = RobotTimeDiff/RealTimeDiff;
										RealTimeDiff = 1;
									}
									else
									{
										RealTimeDiff = RealTimeDiff/RobotTimeDiff;
										RobotTimeDiff = 1;									
									}
									
									console.log(RobotInitX+"=RobotInitX,  "+RobotInitY+"=RobotInitY,  "+RobotEndX+"=RobotEndX,  "+RobotEndY+"=RobotEndY,  "+RobotTimeDiff+"=RobotTime,  "+RobotAngle+"=RobotAngle");
									console.log(RealInitX+"=RealInitX,  "+RealInitY+"=RealInitY,  "+RealEndX+"=RealEndX,  "+RealEndY+"=RealEndY,  "+RealTimeDiff+"=RealTime,  "+RealAngle+"=RealAngle");
									//RobotPlotData = [[0, 0.20], [1, 0.7120], [2, 0.205], [3, 0.19], [4, 0.31900], [5, 0.51900]];//Real
									ComparisionOrig = [[0, RealInitX], [1, RealInitY], [2, RealEndX], [3, RealEndY], [4, RealTimeDiff]];//, [5, RealAngle]];//Imposter
									ComparisionRobot = [[0, RobotInitX], [1, RobotInitY], [2, RobotEndX], [3, RobotEndY], [4, RobotTimeDiff]];//, [5, RobotAngle]];//Imposter
									PlotData();
								},
								error : function(jqXHR, textStatus, errorThrown)
								{
									console.log("There was an error: "+errorThrown);
								},
								dataType: "json"
						});					
					}
				}
				GetDataFromServer("Real");
				GetDataFromServer("Robot");
				var BackgroundColor = "#000000";

				function PlotData()
				{
					var SwipesOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						yaxis: {
							min: 0,
							max: 1080
						},
						xaxis: {
							min: 0,
							max: 1920
						},
						grid: 
						{
            	aboveData: true,
            	backgroundColor: BackgroundColor,
        		}				
			
					};

					ClassifierSeriesObj = function() 
					{
						 return [
							{
								data: ComparisionOrig,
                bars: {
                    show: true,
                    barWidth: 0.15,
                    order: 2
                },
							}, 
							{
								data: ComparisionRobot,
                bars: {
                    show: true,
                    barWidth: 0.15,
                    order: 2
                },
							}, 
						];
					}

					var ComparisionOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						yaxis: {
							min: 0,
							max: 1
						},
						xaxis: {
							ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time']],
							min: -1,
							max: 6
						},
						grid: 
						{
            	aboveData: true,
            	backgroundColor: BackgroundColor,
        		}				
			
					}

					$.plot("#ComparisionPlaceholder", ClassifierSeriesObj(), ComparisionOptions);
					
					console.log(OrigPlotData.toString());
					console.log(RobotPlotData.toString());
					$.plot("#OriginalSwipePlaceholder", [ OrigPlotData], SwipesOptions);
					$.plot("#ForgedSwipePlaceholder", [ RobotPlotData ], SwipesOptions);
					//console.log(OrigPlotData.toString());
				
				}
/*
				$(function() 
				{
					console.log("Index = "+RealXVals.length)
					//var OrigPlotData = [[900, 120], [708, 120], [1070, 205], [1020, 1900]];
					//RobotPlotData = [[0, 0.20], [1, 0.7120], [2, 0.205], [3, 0.19], [4, 0.31900], [5, 0.51900]];//Real
					ComparisionOrig = [[0, 0.12], [1, 0.6120], [2, 0.325], [3, 0.19], [4, 0.31900], [5, 0.51900]];//Imposter
					ComparisionRobot = [[900, 120], [708, 120], [1070, 205], [1020, 1900]];
					var SwipesOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						yaxis: {
							min: 0,
							max: 1920
						},
						xaxis: {
							min: 0,
							max: 1080
						},
						grid: 
						{
            	aboveData: true,
            	backgroundColor: BackgroundColor,
        		}				
			
					}

					ClassifierSeriesObj = function() 
					{
						 return [
							{
								data: RobotPlotData,
                bars: {
                    show: true,
                    barWidth: 0.03,
                    order: 2
                },
							}, 
							{
								data: ComparisionOrig,
                bars: {
                    show: true,
                    barWidth: 0.03,
                    order: 2
                },
							}, 
						];
					}

					var ComparisionOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						yaxis: {
							min: 0,
							max: 1
						},
						xaxis: {
							ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time'],[5,'Angle']],
							min: -1,
							max: 6
						},
						grid: 
						{
            	aboveData: true,
            	backgroundColor: BackgroundColor,
        		}				
			
					}

					//ComparisionPlot = $.plot("#ComparisionPlaceholder", ClassifierSeriesObj(), ComparisionOptions);
					OrigSwipePlot = $.plot("#OriginalSwipePlaceholder", [ OrigPlotData], SwipesOptions);
					ForgedPlot = $.plot("#ForgedSwipePlaceholder", [ RobotPlotData ], SwipesOptions);
					
					GetDataFromServer("Real");
					GetDataFromServer("Robot");
	
				});

				var TotalLoop = 0;
				function UpdateClassifierPlot() 
				{
					ComparisionPlot.setData(ClassifierSeriesObj());
					// Since the axes don't change, we don't need to call plot.setupGrid()
					console.log(OrigPlotData.toString());
					ComparisionPlot.draw();
					if (TotalLoop<4)
						setTimeout(UpdateClassifierPlot, 1000);
					TotalLoop++;
				}
*/
			</script>

	</body>
</html>
