<!DOCTYPE html>
<html>
	<head>
		<title>Authentication and attack demo</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	<head>
	
	<body>
		<div id="CompleteContainer">
		<h1>Robotic attack and authentication demo</h1>
			<ul id="DemoList">
				<li class="EachItem"><a href="nao"><img class="NAO" src="nao.png"/><span>NAO Attack</span></a></li>
				<li class="EachItem"><a href="lego"><img class="EV3" src="ev3.png"/><span>EV3 Attack</span></a></li>
				<li class="EachItem"><a href="sensors"><img class="ACC" src="defense.png"/><span>Defence</span></a></li>
			</ul>
		</div>
		<div class="Clear"></div>
	</body>
</html>
