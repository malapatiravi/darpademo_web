<?
	$TrainUsers = ["User1", "User2", "User3", "User4", "User5"];
	$TestUsers = ["User1", "User2", "User3", "User4", "User5"];
	$TotalUsers = 7;
	$CurrentTrainUser = (isset($_GET['TrUser'])?$_GET['TrUser']:$TestUsers[0]);
	$CurrentTestUser = (isset($_GET['TeUser'])?$_GET['TeUser']:$TestUsers[1]);
?>
<!DOCTYPE html>
<html>
 	<head>
 		<title>Active Authentication Based On Body Movement Patterns</title>
 		<script type="text/javascript" src="../jquery-1.11.0.min.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.js"></script>
		<script language="JavaScript" type="text/javascript" src="../flot/curvedLines.js"></script>
		<link href="style.css" rel="stylesheet" type="text/css">
 	</head>
	<body>
		<div id="CompleteContainer">
			<div id="Header"><h1>Active Authentication Based On Swipe Patterns</h1></div>
			<div class="Clear"><hr/></div>
			<div id="HeadContainer">
				<div class="UsersList" id="TrainDiv">
					<label>Enrolled Users:</label><br/>
						<select id="TrainUserMenu" class="UserSelection">
							<?
								for ($i = 1; $i <= $TotalUsers; $i++)
								{
									$UserVal = "User".$i;
							?>
									<option <?=($CurrentTrainUser==$UserVal)?"selected":""?> value="<?=$UserVal?>"><?=$UserVal?></option>
							<?	
								}
							?>
						</select>			
				</div>
				<div class="UsersList" id="TestDiv">
					<label>Authenticating Users:</label><br/>
						<select id="TestMenu" class="UserSelection">
							<?
								for ($i = 1; $i <= $TotalUsers; $i++)
								{
									$UserVal = "User".$i;
							?>
									<option <?=($CurrentTestUser==$UserVal)?"selected":""?> value="<?=$UserVal?>"><?=$UserVal?></option>
							<?	
								}
							?>
						</select>			
				</div>
			</div>

			<div class="Clear"><br/></div>
			<div id="InitializeButtonSpan"><input type="button" id="StartData" class="StartBtn" value="Authenticate"/></div>
			<div class="Clear"><hr/></div>
			<div id="showdata"></div>
			<div id="ScoreVal"></div>

			<div class="Clear"></div>
			<table class="PlotContainerTable">
				<tr class="PlotContainerTableTR">
					<td class="PlotContainerTableTD1">
						<div>Similarity Score</div>
					</td>
					
					<td class="PlotContainerTableTD2">
						<div class="ClassifierPlotHolder" style="	margin-top:30px;">Classifier Score</div><div id="ClassifierScorePlot"></div>					
					</td>
				</tr>
				<tr class="PlotContainerTableTR">
					<td colspan=2>
						Time
					</td>
				</tr>
			</table>
			<script>
				var CurrentSelectedTrain = "<?=$CurrentTrainUser;?>"; 
				var CurrentSelectedTest = "<?=$CurrentTestUser;?>"; 
				var LoopInterval = 10;
				var Threshold = 0.25;
				var BackgroundColor = "#000000";
				
				var AllUserNames = CurrentSelectedTrain+","+CurrentSelectedTest;
				var IntLineNumbers = new Array();
				var TotalForEach = new Array();
				var StringLineNumber = "";
				var ReadStatus = "1,1";
				var TotalUsers = 2;
				var DemoFolder = "DEMO/swipeauthentication/Datum";
				var Mode = "Training";
				var Pause = false;
				var Counter = 0;
				var RunAjax;
				var XAxisData = new Array();
				var YAxisData = new Array();
				var ZAxisData = new Array();
				var MData = new Array();
				var ClassifierData = new Array();
				var SwipeTypeData = new Array();
				var Users = new Array();
				var ScoreFilename = "";
				var ScoreFolder = DemoFolder+"/Scores";
				$('#TrainUserMenu').on('change', function()
				{
					CurrentSelectedTrain = $('#TrainUserMenu').find(":selected").text().replace(/\s/g, '');
					AllUserNames = CurrentSelectedTrain+","+CurrentSelectedTest;
					AllUserNames = AllUserNames.replace(/\s/g, '');
					console.log("New all user names = "+AllUserNames);
					location.href = "?TrUser="+CurrentSelectedTrain+"&TeUser="+CurrentSelectedTest;
				});
				$('#TestMenu').on('change', function()
				{
					CurrentSelectedTest = $('#TestMenu').find(":selected").text().replace(/\s/g, '');
					AllUserNames = CurrentSelectedTrain+","+CurrentSelectedTest;
					AllUserNames = AllUserNames.replace(/\s/g, '');
					console.log("New all user names = "+AllUserNames);
					location.href = "?TrUser="+CurrentSelectedTrain+"&TeUser="+CurrentSelectedTest;
				});

				Users = AllUserNames.split(",");
				for (i=0; i<TotalUsers;i++)
				{
					TotalForEach[i] = 0;
					IntLineNumbers[i] = 0;
					XAxisData[i] = new Array();
					YAxisData[i] = new Array();
					ZAxisData[i] = new Array();
					MData[i] = new Array();
					ClassifierData[i] = new Array();
					SwipeTypeData[i] = new Array();
					StringLineNumber = (i==0)?IntLineNumbers[i]:(""+StringLineNumber+","+IntLineNumbers[i]);
					ScoreFilename += ((i==0)?"":"-")+Users[i];
				}
				ScoreFilename+=".txt";
				console.log("Scorefile name = "+ScoreFilename+" in "+ScoreFolder);
				$(document).ready(function()
				{
						$('#StartData').on('click', function()
						{
							/*
							$.ajax(
							{
                type: "POST",
                url: "",
                data: {CurrentTr: CurrentSelectedTrain, CurrentTe: CurrentSelectedTest},
                success: function(){ alert('reloading page'); location.reload }
              });
              */
              
              RunAjax = setInterval("GetDataFromServer()", LoopInterval);
							RunClassifierScore = setInterval("GetClassifierScore()", LoopInterval);
						});					
				});
				function GetDataFromServer()
				{
					if (!Pause)
					{
						$.ajax({
							type: 'POST',
							url: "DataReceiver.php",
							data: {LineNumbers: StringLineNumber, Usernames: AllUserNames, DataMode: Mode, DemoF: DemoFolder, Statuses: ReadStatus},
							success: function(ServerData)
							{
									//$('#showdata').html("<p>Line number="+ServerData.LineNumber+" Data="+ServerData.Data[1]+"</p>");
									var GetStatuses = ServerData.Statuses.split(",");
									for (i=0; i<TotalUsers;i++)
									{
										var SplittedData = ServerData.Data[i].split(",");
										if (GetStatuses[i]!="0")
										{
											TotalForEach[i] = TotalForEach[i]+1;
											IntLineNumbers[i] = IntLineNumbers[i]+1;
											XAxisData[i].push(SplittedData[0]);
											YAxisData[i].push(SplittedData[1]);
											ZAxisData[i].push(SplittedData[2]);
											var MValue = Math.pow((Math.pow(SplittedData[0],2)+Math.pow(SplittedData[1],2)+Math.pow(SplittedData[2],2)),0.5)
											MData[i].push(MValue);
										}
										StringLineNumber = (i==0)?IntLineNumbers[i]:(""+StringLineNumber+","+IntLineNumbers[i]);
										console.log("Total for "+i+"="+TotalForEach[i]);
									}
							},
							error : function(jqXHR, textStatus, errorThrown)
							{
								console.log("There was an error: "+errorThrown+" variables: StringLineNumber="+StringLineNumber+", AllUserNames="+AllUserNames+", Mode="+Mode+", Folder="+DemoFolder+", Status="+ReadStatus);
							},
							dataType: "json"
						});
					}
				}
				var CurrentScoreLine = 0;
				var ScoreFileStatus = 1;
				var ScoreVal = 50;
				var PrevScoreVal = 50;

				function GetClassifierScore()
				{
					$.ajax({
						type: 'POST',
						url: "ScoreReceiver.php",
						data: {CurrentLineNumber: CurrentScoreLine, Status: ScoreFileStatus, ScrFolder: ScoreFolder, ScrFile: ScoreFilename, PreviousScoreValue: PrevScoreVal},
						success: function(ServerData)
						{
							ScoreFileStatus = parseInt(ServerData.Status);
							if (ScoreFileStatus!=0)
							{
								ScoreVal = ServerData.Data;
								PrevScoreVal = parseFloat(ScoreVal);
								ClassifierData[0].push(Threshold);
								ClassifierData[1].push(ScoreVal);
								CurrentScoreLine = CurrentScoreLine+1;//intval(ServerData.LineNumbers);
								//$('#ScoreVal').html("<p>Line number="+CurrentScoreLine+" Value="+ScoreVal+", Status = "+ScoreFileStatus+"</p>");							
							}
						},
						error : function(jqXHR, textStatus, errorThrown)
						{
							console.log("There was an error: "+errorThrown+" variables: StringLineNumber="+CurrentScoreLine+", AllUserNames="+AllUserNames+", Mode="+Mode+", Folder="+DemoFolder+", Status="+ScoreFileStatus+", PrevVal="+PrevScoreVal);
						},
						dataType: "json"
					});
				} 
				
				

				var XAxisPlotCounter = 0;
				var YAxisPlotCounter = 0;
				var ZAxisPlotCounter = 0;
				var MPlotCounter = 0;
				var ClassifierPlotCounter = 0;

				$(function()
				{
					var XAxisGData = new Array(), 		XAxisTotalPoints = 200;
					var YAxisGData = new Array(), 		YAxisTotalPoints = 200;
					var ZAxisGData = new Array(), 	ZAxisTotalPoints = 200;
					var MGData = new Array(), 			MTotalPoints = 200;
					var ClassifierGData = new Array(), 			ClassifierTotalPoints = 20;
					var updateInterval = 50;
					$("#updateInterval").val(updateInterval).change(function () 
					{
						var v = $(this).val();
						if (v && !isNaN(+v)) {
							updateInterval = +v;
							if (updateInterval < 1) {
								updateInterval = 1;
							} else if (updateInterval > 2000) {
								updateInterval = 2000;
							}
							$(this).val("" + updateInterval);
						}
					});
					var ClassifierUpdateInterval = 400;
					$("#ClassifierUpdateInterval").val(ClassifierUpdateInterval).change(function () 
					{
						var v = $(this).val();
						if (v && !isNaN(+v)) {
							ClassifierUpdateInterval = +v;
							if (ClassifierUpdateInterval < 1) {
								ClassifierUpdateInterval = 1;
							} else if (ClassifierUpdateInterval > 2000) {
								ClassifierUpdateInterval = 2000;
							}
							$(this).val("" + ClassifierUpdateInterval);
						}
					});					
					
					for (i=0; i<TotalUsers;i++)
					{
						XAxisGData[i] = new Array();
						YAxisGData[i] = new Array();
						ZAxisGData[i] = new Array();
						MGData[i] = new Array();
						ClassifierGData[i] = new Array();
					}

				/******************************************************************************/
				//Classifier data graph
				/******************************************************************************/

					function GetClassifierData(UserNumber)
					{
						if (ClassifierGData[UserNumber].length > 0)
							ClassifierGData[UserNumber] = ClassifierGData[UserNumber].slice(1);

						// Do a random walk

						while (ClassifierGData[UserNumber].length < ClassifierTotalPoints)
						{
							if (ClassifierData[UserNumber].length > ClassifierPlotCounter)
							{
									ClassifierGData[UserNumber].push(ClassifierData[UserNumber][ClassifierPlotCounter]);
									//console.log(XAxisData[XAxisPlotCounter]);
									ClassifierPlotCounter++;
							}
							else
								ClassifierGData[UserNumber].push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < ClassifierGData[UserNumber].length; ++i) {
							if (UserNumber==1)
								res.push([i, ClassifierGData[UserNumber][i]]);
							else
								res.push([i, Threshold]);							
						}
						return res;
						
					}

					/******************************************************************************/				
					ClassifierSeriesObj = function() 
					{
						 return [
							{
								data: GetClassifierData(0),
								lines: { show: true, fill: false }
							}, 
							{
								data: GetClassifierData(1),
								points: { show: true, radius: 2, lineWidth: 4, fill: false },
								lines: { show: true, fill: false }
							},
						];
					}
					var ClassifierOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						yaxis: {
							min: 0,
							max: 1.0,
							label: "Scores"
						},
						xaxis: {
							show: false
						},
						grid: 
						{
            	aboveData: true,
            	backgroundColor: BackgroundColor,
        		}				
			
					}

					/******************************************************************************/
					var ClassifierPlot = $.plot("#ClassifierScorePlot", ClassifierSeriesObj(), ClassifierOptions);
					/******************************************************************************/
					function UpdateClassifierPlot() 
					{
						ClassifierPlot.setData(ClassifierSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						ClassifierPlot.draw();
						setTimeout(UpdateClassifierPlot, ClassifierUpdateInterval);
					}
					UpdateClassifierPlot();
					/******************************************************************************/
					/******************************************************************************/
					$(".flot-text").css('color','#22ff22 !important');
				});	
				
			</script>
		</div>
		
	</body>
</html>
