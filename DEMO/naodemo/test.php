<!DOCTYPE html>
<html>
 	<head>
 		<title>Authentication defence</title>
 		<script type="text/javascript" src="../jquery-1.11.0.min.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.js"></script>
		<script language="JavaScript" type="text/javascript" src="../flot/curvedLines.js"></script>
		<link href="style.css" rel="stylesheet" type="text/css">
 	</head>
	<body>
		<div id="CompleteContainer">
			<div id="Header"><span class="Image"><img src="../nao.png"/></span><span class="Title"><h1>Authentication attack from NAO</h1></span><span class="Image"><a href="../"><img class="BackBtn" src="../back.png"/></a></span></span></div>
			<div class="Clear"><hr/></div>
			<div id="GraphContainer">
				<input type="button" id="StartData" class="StartBtn" value="Initialize"/>
			</div>
			<div class="Clear"><hr/></div>
	 		<hr/>
			<div id="showdata"></div>
	 		<div id="PlotsContainer">
	 			<ul id="PlotsUL">
	 				<!--<li><div class="GraphLabel">X-Y Screen</div><div id="AxisPlot"></div></li>-->
	 				<li><div class="GraphLabel">X Axis</div><div id="XAxisPlot"></div></li>
	 				<li><div class="GraphLabel">Y Axis</div><div id="YAxisPlot"></div></li>
	 				<li><div class="GraphLabel">Area</div><div id="AreaPlot"></div></li>
	 				<li><div class="GraphLabel">Pressure</div><div id="PressurePlot"></div></li>
	 				<li><div class="GraphLabel">Z-Score</div><div id="ZScorePlot"></div></li>
	 			</ul>
	 		</div>
			
			
			
			
			<script>
				var IndexOfX = 3;
				var IndexOfY = 4;
				var IndexOfArea = 6;
				var IndexOfPressure = 5;
				var IndexOfZScore = 0;
				var IndexOfActionType = 1;
				console.log("test");
			</script>
			<script src="AreaPlot.js" ></script>
		</div>
	</body>
</html>  
