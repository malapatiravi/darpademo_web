				/******************************************************************************/
				//Classifier score data graph
				/******************************************************************************/

					function GetClassifierScoreData()
					{
						if (ClassifierScoreGData.length > 0)
							ClassifierScoreGData = ClassifierScoreGData.slice(1);

						// Do a random walk

						while (ClassifierScoreGData.length < ClassifierScoreTotalPoints)
						{
							if (ClassifierScoreData.length > ClassifierScorePlotCounter)
							{
								ClassifierScoreGData.push(ClassifierScoreData[ClassifierScorePlotCounter]);
								ClassifierScorePlotCounter++;
							}
							else
								ClassifierScoreGData.push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < ClassifierScoreGData.length; ++i) {
							if (ClassifierScoreGData[i]!=0) console.log("Adding val = "+ClassifierScoreGData[i]);
								res.push([i, ClassifierScoreGData[i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					ClassifierScoreSeriesObj = function() 
					{
						 return [
							{
								data: GetClassifierScoreData(),
								lines: { show: true, fill: false },
								points: { show: true, radius: 2, lineWidth: 4, fill: false },
								color: User1Color
							},
							{
								data: [[0,70],[29, 70]],
								lines: { show: true, fill: false },
								color: "green"
							},
						];
					}
					var ClassifierScoreOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -2,
							max: 15
						},
						xaxis: {
							show: false
						},					
						yaxis: {
							min: -10,
							max: 110
						}					

					}

					/******************************************************************************/
					var ClassifierScorePlot = $.plot("#ClassifierScorePlot", ClassifierScoreSeriesObj(), ClassifierScoreOptions);
					/******************************************************************************/
					function UpdateClassifierScorePlot() 
					{
						ClassifierScorePlot.setData(ClassifierScoreSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						ClassifierScorePlot.draw();
						setTimeout(UpdateClassifierScorePlot, SecondaryInterval);
					}
					UpdateClassifierScorePlot();
					/******************************************************************************/
					/******************************************************************************/
