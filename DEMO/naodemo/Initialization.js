				var IndexOfX = 3;
				var IndexOfY = 4;
				var IndexOfArea = 6;
				var IndexOfPressure = 5;
				var IndexOfClassifierScore = 0;
				var IndexOfActionType = 1;
				var SelectedUser = "";
				var AllUserNames = "";
				var LoopInterval = 10;
				var IntLineNumbers = new Array();
				var TotalForEach = new Array();
				var StringLineNumber = "";
				var ReadStatus = "1,1";
				var TotalUsers = 2;
				var DemoFolder = "DEMO/nao/Datum";
				var PrevScoreVal = 50;
				var Mode = "Training";
				var Pause = false;
				var Counter = 0;
				var RunAjax;
				var AxisData = new Array();
				var XAxisData = new Array();
				var YAxisData = new Array();
				var ActionTypeData = new Array();
				var AreaData = new Array();
				var PressureData = new Array();
				var ClassifierScoreData = new Array();
				var User1Color = "#003D69";
				var User2Color = "#BA0000";
				var SwipeTypeData = new Array();
				var Users = new Array();
				
				$('#SelectUserMenu').on('change', function()
				{
					SelectedUser = $('#SelectUserMenu').find(":selected").text().replace(/\s/g, '');
					AllUserNames = "NAOSwipe,"+SelectedUser;
					AllUserNames = AllUserNames.replace(/\s/g, '');
					console.log("New all user names = "+AllUserNames);
					location.href = "?CurrentUser="+SelectedUser;
				});
				for (i=0; i<TotalUsers;i++)
				{
					TotalForEach[i] = 0;
					IntLineNumbers[i] = 0;
					AxisData[i] = new Array();
					XAxisData[i] = new Array();
					YAxisData[i] = new Array();
					ActionTypeData[i] = new Array();
					AreaData[i] = new Array();
					PressureData[i] = new Array();
					ClassifierScoreData[i] = new Array();
					
					SwipeTypeData[i] = new Array();
					StringLineNumber = (i==0)?IntLineNumbers[i]:(""+StringLineNumber+","+IntLineNumbers[i]);
				}
				$(document).ready(function()
				{
						$('#StartData').on('click', function()
						{
							RunAjax = setInterval("GetDataFromServer()", LoopInterval);
							RunAjax = setInterval("GetClassifierScore()", 500);
						});					
				});
				function GetDataFromServer()
				{
					if (!Pause)
					{
						$.ajax({
							type: 'POST',
							url: "DataReceiver.php",
							data: {LineNumbers: StringLineNumber, Usernames: AllUserNames, DataMode: Mode, DemoF: DemoFolder, Statuses: ReadStatus},
							success: function(ServerData)
							{
									var GetStatuses = ServerData.Statuses.split(",");
									for (i=0; i<TotalUsers;i++)
									{
										var SplittedData = ServerData.Data[i].split(",");
										//$('#showdata').html("<p>Line number="+ServerData.LineNumbers+" Data="+ServerData.Data[i]+"</p>");
										if (GetStatuses[i]!="0")
										{
											TotalForEach[i] = TotalForEach[i]+1;
											IntLineNumbers[i] = IntLineNumbers[i]+1;

											AxisData[i].push(SplittedData[0]);
											XAxisData[i].push(SplittedData[IndexOfX]);
											YAxisData[i].push(SplittedData[IndexOfY]);
											ActionTypeData[i].push(SplittedData[IndexOfActionType]);
											AreaData[i].push(SplittedData[IndexOfArea]);
											PressureData[i].push(SplittedData[IndexOfPressure]);
											ClassifierScoreData[i].push(SplittedData[IndexOfClassifierScore]);

											//PressureData[i].push(SplittedData[2]);
											//var MValue = Math.pow((Math.pow(SplittedData[0],2)+Math.pow(SplittedData[1],2)+Math.pow(SplittedData[2],2)),0.5)
											//MData[i].push(MValue);
										}
										StringLineNumber = (i==0)?IntLineNumbers[i]:(""+StringLineNumber+","+IntLineNumbers[i]);
										//console.log("Total for "+i+"="+TotalForEach[i]);
									}
							},
							error : function(jqXHR, textStatus, errorThrown)
							{
								console.log("There was an error: "+errorThrown+" variables: StringLineNumber="+StringLineNumber+", AllUserNames="+AllUserNames+", Mode="+Mode+", Folder="+DemoFolder+", Status="+ReadStatus);
							},
							dataType: "json"
						});
					}
				} 
				
				var CurrentScoreLine = 0;
				var ScoreFileStatus = 1;
				var ScoreVal = 50;
				function GetClassifierScore()
				{
					$.ajax({
						type: 'POST',
						url: "ScoreReceiver.php",
						data: {CurrentLineNumber: CurrentScoreLine, Status: ScoreFileStatus, ScrFolder: DemoFolder, PreviousScoreValue: PrevScoreVal},
						success: function(ServerData)
						{
							ScoreFileStatus = parseInt(ServerData.Status);
							if (ScoreFileStatus!=0)
							{
								ScoreVal = ServerData.Data;
								ClassifierScoreData.push(ScoreVal);
								PrevScoreVal = parseInt(ScoreVal);
								CurrentScoreLine = CurrentScoreLine+1;//intval(ServerData.LineNumbers);
								//$('#ScoreVal').html("<p>Line number="+CurrentScoreLine+" Value="+ScoreVal+", Status = "+ScoreFileStatus+"</p>");							
							}
						},
						error : function(jqXHR, textStatus, errorThrown)
						{
							console.log("There was an error: "+errorThrown+" variables: StringLineNumber="+StringLineNumber+", AllUserNames="+AllUserNames+", Mode="+Mode+", Folder="+DemoFolder+", Status="+ReadStatus);
						},
						dataType: "json"
					});
				} 
				
				var AxisPlotCounter = 0;
				var XAxisPlotCounter = 0;
				var YAxisPlotCounter = 0;
				var AxisGData = new Array(), 		AxisTotalPoints = 200;
				var XAxisGData = new Array(), 		XAxisTotalPoints = 200;
				var YAxisGData = new Array(), 		YAxisTotalPoints = 200;
				var ActionTypeGData = new Array(), 		ActionTypeTotalPoints = 200;
				var AreaGData = new Array(), 		AreaTotalPoints = 200;
				var PressureGData = new Array(), 	PressureTotalPoints = 200;
				var ClassifierScoreGData = new Array(), 			ClassifierScoreTotalPoints = 30;
								
				var ActionTypePlotCounter = 0;
				var AreaPlotCounter = 0;
				var PressurePlotCounter = 0;
				var ClassifierScorePlotCounter = 0;
				var updateInterval = 50;
				var SecondaryInterval = 500;
				
				for (i=0; i<TotalUsers;i++)
				{
					AxisGData[i] = new Array();
					XAxisGData[i] = new Array();
					YAxisGData[i] = new Array();
					ActionTypeGData[i] = new Array();
					AreaGData[i] = new Array();
					PressureGData[i] = new Array();
				}
				

