<!DOCTYPE html>
<html>
 	<head>
 		<title>Authentication defence</title>
 		<script type="text/javascript" src="../jquery-1.11.0.min.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.js"></script>
		<script language="JavaScript" type="text/javascript" src="../flot/curvedLines.js"></script>
		<link href="style.css" rel="stylesheet" type="text/css">
 	</head>
	<body>
		<div id="CompleteContainer">
			<div id="Header"><span class="Image"><img src="../nao.png"/></span><span class="Title"><h1>Authentication attack from NAO</h1></span><span class="Image"><a href="../"><img class="BackBtn" src="../back.png"/></a></span></span></div>
			<div class="Clear"><hr/></div>
			<div id="GraphContainer">
				<input type="button" id="StartData" class="StartBtn" value="Initialize"/>
			</div>
			<div class="Clear"><hr/></div>
	 		<hr/>
			<div id="showdata"></div>
	 		<div id="PlotsContainer">
	 			<ul id="PlotsUL">
	 				<li><div class="GraphLabel">X-Y Screen</div><div id="AxisPlot"></div></li>
	 				<li><div class="GraphLabel">Area</div><div id="AreaPlot"></div></li>
	 				<li><div class="GraphLabel">Pressure</div><div id="PressurePlot"></div></li>
	 				<li><div class="GraphLabel">Z-Score</div><div id="ZScorePlot"></div></li>
	 			</ul>
	 		</div>
			
			
			
			
			<script>
				var IndexOfX = 3;
				var IndexOfY = 4;
				var IndexOfArea = 6;
				var IndexOfPressure = 5;
				var IndexOfZScore = 0;

				var LoopInterval = 10;
				var AllUserNames = "Legouser1,ev3user1";
				var IntLineNumbers = new Array();
				var TotalForEach = new Array();
				var StringLineNumber = "";
				var ReadStatus = "1,1";
				var TotalUsers = 2;
				var DemoFolder = "DEMO/nao/Datum";
				var Mode = "Training";
				var Pause = false;
				var Counter = 0;
				var RunAjax;
				var AxisData = new Array();
				var XAxisData = new Array();
				var YAxisData = new Array();
				var AreaData = new Array();
				var PressureData = new Array();
				var ZScoreData = new Array();

				var SwipeTypeData = new Array();
				var Users = new Array();
				for (i=0; i<TotalUsers;i++)
				{
					TotalForEach[i] = 0;
					IntLineNumbers[i] = 0;
					Users = AllUserNames.split(",");
					AxisData[i] = new Array();
					XAxisData[i] = new Array();
					YAxisData[i] = new Array();
					AreaData[i] = new Array();
					PressureData[i] = new Array();
					ZScoreData[i] = new Array();
					
					SwipeTypeData[i] = new Array();
					StringLineNumber = (i==0)?IntLineNumbers[i]:(""+StringLineNumber+","+IntLineNumbers[i]);
				}
				$(document).ready(function()
				{
						$('#StartData').on('click', function()
						{
							RunAjax = setInterval("GetDataFromServer()", LoopInterval);
						});					
				});
				function GetDataFromServer()
				{
					if (!Pause)
					{
						$.ajax({
							type: 'POST',
							url: "DataReceiver.php",
							data: {LineNumbers: StringLineNumber, Usernames: AllUserNames, DataMode: Mode, DemoF: DemoFolder, Statuses: ReadStatus},
							success: function(ServerData)
							{
									var GetStatuses = ServerData.Statuses.split(",");
									for (i=0; i<TotalUsers;i++)
									{
										var SplittedData = ServerData.Data[i].split(",");
										$('#showdata').html("<p>Line number="+ServerData.LineNumbers+" Data="+ServerData.Data[i]+"</p>");
										if (GetStatuses[i]!="0")
										{
											TotalForEach[i] = TotalForEach[i]+1;
											IntLineNumbers[i] = IntLineNumbers[i]+1;

											AxisData[i].push(SplittedData[0]);
											XAxisData[i].push(SplittedData[IndexOfX]);
											YAxisData[i].push(SplittedData[IndexOfY]);
											AreaData[i].push(SplittedData[IndexOfArea]);
											PressureData[i].push(SplittedData[IndexOfPressure]);
											ZScoreData[i].push(SplittedData[IndexOfZScore]);

											//PressureData[i].push(SplittedData[2]);
											//var MValue = Math.pow((Math.pow(SplittedData[0],2)+Math.pow(SplittedData[1],2)+Math.pow(SplittedData[2],2)),0.5)
											//MData[i].push(MValue);
										}
										StringLineNumber = (i==0)?IntLineNumbers[i]:(""+StringLineNumber+","+IntLineNumbers[i]);
										console.log("Total for "+i+"="+TotalForEach[i]);
									}
							},
							error : function(jqXHR, textStatus, errorThrown)
							{
								console.log("There was an error: "+errorThrown+" variables: StringLineNumber="+StringLineNumber+", AllUserNames="+AllUserNames+", Mode="+Mode+", Folder="+DemoFolder+", Status="+ReadStatus);
							},
							dataType: "json"
						});
					}
				}
				
				var AxisPlotCounter = 0;
				var XAxisPlotCounter = 0;
				var YAxisPlotCounter = 0;
				var AreaPlotCounter = 0;
				var PressurePlotCounter = 0;
				var ZScorePlotCounter = 0;


				$(function()
				{
					var AxisGData = new Array(), 		AxisTotalPoints = 200;
					var XAxisGData = new Array(), 		XAxisTotalPoints = 200;
					var YAxisGData = new Array(), 		YAxisTotalPoints = 200;
					var AreaGData = new Array(), 		AreaTotalPoints = 200;
					var PressureGData = new Array(), 	PressureTotalPoints = 200;
					var ZScoreGData = new Array(), 			ZScoreTotalPoints = 200;
					var updateInterval = 50;
					$("#updateInterval").val(updateInterval).change(function () 
					{
						var v = $(this).val();
						if (v && !isNaN(+v)) {
							updateInterval = +v;
							if (updateInterval < 1) {
								updateInterval = 1;
							} else if (updateInterval > 2000) {
								updateInterval = 2000;
							}
							$(this).val("" + updateInterval);
						}
					});
					for (i=0; i<TotalUsers;i++)
					{
						AxisGData[i] = new Array();
						XAxisGData[i] = new Array();
						YAxisGData[i] = new Array();
						AreaGData[i] = new Array();
						PressureGData[i] = new Array();
						ZScoreGData[i] = new Array();
					}

				/******************************************************************************/
				//XY-Axis data graph
				/******************************************************************************/

					function GetAxisData(UserNumber)
					{
						if (AxisGData[UserNumber].length > 0)	AxisGData[UserNumber] = AxisGData[UserNumber].slice(1);
						if (XAxisGData[UserNumber].length > 0)	XAxisGData[UserNumber] = XAxisGData[UserNumber].slice(1);
						if (YAxisGData[UserNumber].length > 0)	YAxisGData[UserNumber] = YAxisGData[UserNumber].slice(1);
						// Do a random walk

						while (AxisGData[UserNumber].length < AxisTotalPoints)
						{
							if (AxisData[UserNumber].length > AxisPlotCounter)
							{
									AxisGData[UserNumber].push(AxisData[UserNumber][AxisPlotCounter]);
									AxisPlotCounter++;
							}
							else AxisGData[UserNumber].push(0);
						}

						while (XAxisGData[UserNumber].length < XAxisTotalPoints)
						{
							if (XAxisData[UserNumber].length > XAxisPlotCounter)
							{
									XAxisGData[UserNumber].push(XAxisData[UserNumber][XAxisPlotCounter]);
									XAxisPlotCounter++;
							}
							else XAxisGData[UserNumber].push(0);
						}

						while (YAxisGData[UserNumber].length < YAxisTotalPoints)
						{
							if (YAxisData[UserNumber].length > YAxisPlotCounter)
							{
									YAxisGData[UserNumber].push(YAxisData[UserNumber][YAxisPlotCounter]);
									YAxisPlotCounter++;
							}
							else YAxisGData[UserNumber].push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < AxisGData[UserNumber].length; ++i) {
							res.push([XAxisGData[UserNumber][i], YAxisGData[UserNumber][i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					AxisSeriesObj = function() 
					{
						//var CurrentData1 = [GetAxisData(0),GetPressureData(0)];
						//var CurrentData2 = [GetAxisData(1),GetPressureData(1)];
						var CurrentData1 = [[1000, 100],[500, 500],[200, 200],[100, 100]];
						var CurrentData2 = [500,500];
						 return [
							{
								data: GetAxisData(0),
								lines: { show: true, fill: false },
								label: Users[0]
							}, 
							{
								data: GetAxisData(1),
								lines: { show: true, fill: false },
								label: Users[1]
							},
						];
					}
					var AxisOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -3,
							max: 5
						},
						xaxis: {
							min: 0,
							max: 1080
						},					
						yaxis: {
							min: 0,
							max: 1920
						},					
					}

					/******************************************************************************/				
					/******************************************************************************/

		 			var AxisPlot = $.plot("#AxisPlot", AxisSeriesObj(), AxisOptions);
					/******************************************************************************/
					function UpdateAxisPlot() 
					{
						AxisPlot.setData( AxisSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						AxisPlot.draw();
						setTimeout(UpdateAxisPlot, updateInterval);
					}
					UpdateAxisPlot();
					/******************************************************************************/
					/******************************************************************************/




				/******************************************************************************/
				//Y-Axis data graph
				/******************************************************************************/

					function GetAreaData(UserNumber)
					{
						if (AreaGData[UserNumber].length > 0)
							AreaGData[UserNumber] = AreaGData[UserNumber].slice(1);

						// Do a random walk

						while (AreaGData[UserNumber].length < AreaTotalPoints)
						{
							if (AreaData[UserNumber].length > AreaPlotCounter)
							{
									AreaGData[UserNumber].push(AreaData[UserNumber][AreaPlotCounter]);
									//console.log(XAxisData[XAxisPlotCounter]);
									AreaPlotCounter++;
							}
							else
								AreaGData[UserNumber].push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < AreaGData[UserNumber].length; ++i) {
							res.push([i, AreaGData[UserNumber][i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					AreaSeriesObj = function() 
					{
						 return [
							{
								data: GetAreaData(0),
								lines: { show: true, fill: false },
								label: Users[0]
							}, 
							{
								data: GetAreaData(1),
								lines: { show: true, fill: false },
								label: Users[1]
							},
						];
					}
					var AreaOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -3,
							max: 5
						},
						xaxis: {
							show: false
						},					
						yaxis: {
							min: -0.1,
							max: 0.4
						}					
					}

					/******************************************************************************/
					var AreaPlot = $.plot("#AreaPlot", AreaSeriesObj(), AreaOptions);
					/******************************************************************************/
					function UpdateAreaPlot() 
					{
						AreaPlot.setData(AreaSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						AreaPlot.draw();
						setTimeout(UpdateAreaPlot, updateInterval);
					}
					UpdateAreaPlot();
					/******************************************************************************/
					/******************************************************************************/




				/******************************************************************************/
				//Pressure data graph
				/******************************************************************************/

					function GetPressureData(UserNumber)
					{
						if (PressureGData[UserNumber].length > 0)
							PressureGData[UserNumber] = PressureGData[UserNumber].slice(1);

						// Do a random walk

						while (PressureGData[UserNumber].length < PressureTotalPoints)
						{
							if (PressureData[UserNumber].length > PressurePlotCounter)
							{
									PressureGData[UserNumber].push(PressureData[UserNumber][PressurePlotCounter]);
									//console.log(XAxisData[XAxisPlotCounter]);
									PressurePlotCounter++;
							}
							else
								PressureGData[UserNumber].push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < PressureGData[UserNumber].length; ++i) {
							res.push([i, PressureGData[UserNumber][i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					PressureSeriesObj = function() 
					{
						 return [
							{
								data: GetPressureData(0),
								lines: { show: true, fill: false },
								label: Users[0]
							}, 
							{
								data: GetPressureData(1),
								lines: { show: true, fill: false },
								label: Users[1]
							},
						];
					}
					var PressureOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -3,
							max: 5
						},
						xaxis: {
							show: false
						},					
						yaxis: {
							min: -0.1,
							max: 0.8
						}					
					}

					/******************************************************************************/
					var PressurePlot = $.plot("#PressurePlot", PressureSeriesObj(), PressureOptions);
					/******************************************************************************/
					function UpdatePressurePlot() 
					{
						PressurePlot.setData(PressureSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						PressurePlot.draw();
						setTimeout(UpdatePressurePlot, updateInterval);
					}
					UpdatePressurePlot();
					/******************************************************************************/
					/******************************************************************************/

				/******************************************************************************/
				//ZScore data graph
				/******************************************************************************/

					function GetZScoreData(UserNumber)
					{
						if (ZScoreGData[UserNumber].length > 0)
							ZScoreGData[UserNumber] = ZScoreGData[UserNumber].slice(1);

						// Do a random walk

						while (ZScoreGData[UserNumber].length < ZScoreTotalPoints)
						{
							if (ZScoreData[UserNumber].length > ZScorePlotCounter)
							{
									ZScoreGData[UserNumber].push(ZScoreData[UserNumber][ZScorePlotCounter]);
									//console.log(XAxisData[XAxisPlotCounter]);
									ZScorePlotCounter++;
							}
							else
								ZScoreGData[UserNumber].push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < ZScoreGData[UserNumber].length; ++i) {
							res.push([i, ZScoreGData[UserNumber][i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					ZScoreSeriesObj = function() 
					{
						 return [
							{
								data: GetZScoreData(0),
								lines: { show: true, fill: false },
								label: Users[0]
							}, 
							{
								data: GetZScoreData(1),
								lines: { show: true, fill: false },
								label: Users[1]
							},
						];
					}
					var ZScoreOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -2,
							max: 15
						},
						xaxis: {
							show: false
						},					
						yaxis: {
							min: 1,
							max: 20
						}					

					}

					/******************************************************************************/
					var ZScorePlot = $.plot("#ZScorePlot", ZScoreSeriesObj(), ZScoreOptions);
					/******************************************************************************/
					function UpdateZScorePlot() 
					{
						ZScorePlot.setData(ZScoreSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						ZScorePlot.draw();
						setTimeout(UpdateZScorePlot, updateInterval);
					}
					UpdateZScorePlot();
					/******************************************************************************/
					/******************************************************************************/


				});

			</script>
		</div>
	</body>
</html> 
