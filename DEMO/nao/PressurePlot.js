				/******************************************************************************/
				//Pressure data graph
				/******************************************************************************/

					function GetPressureData(UserNumber)
					{
						if (PressureGData[UserNumber].length > 0)
							PressureGData[UserNumber] = PressureGData[UserNumber].slice(1);

						// Do a random walk

						while (PressureGData[UserNumber].length < PressureTotalPoints)
						{
							if (PressureData[UserNumber].length > PressurePlotCounter)
							{
									PressureGData[UserNumber].push(PressureData[UserNumber][PressurePlotCounter]);
									//console.log(XAxisData[XAxisPlotCounter]);
									PressurePlotCounter++;
							}
							else
								PressureGData[UserNumber].push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < PressureGData[UserNumber].length; ++i) {
							res.push([i, PressureGData[UserNumber][i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					PressureSeriesObj = function() 
					{
						 return [
							{
								data: GetPressureData(0),
								lines: { show: true, fill: false },
								color: User1Color
							}, 
							{
								data: GetPressureData(1),
								lines: { show: true, fill: false },
								color: User2Color
							},
						];
					}
					var PressureOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						Area: {
							min: -3,
							max: 5
						},
						xaxis: {
							show: false
						},					
						yaxis: {
							min: -0.1,
							max: 1.0
						}					
					}

					/******************************************************************************/
					var PressurePlot = $.plot("#PressurePlot", PressureSeriesObj(), PressureOptions);
					/******************************************************************************/
					function UpdatePressurePlot() 
					{
						PressurePlot.setData(PressureSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						PressurePlot.draw();
						setTimeout(UpdatePressurePlot, updateInterval);
					}
					UpdatePressurePlot();
					/******************************************************************************/
					/******************************************************************************/

 
