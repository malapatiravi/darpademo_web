<? //$CurrentUser = "Suzzett"; if (isset($_GET['CurrentUser'])) $CurrentUser = $_GET['CurrentUser']?>
<!DOCTYPE html>
<html>
 	<head>
 		<title>Active Authentication </title>
 		<script type="text/javascript" src="../jquery-1.11.0.min.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.js"></script>
		<script language="JavaScript" type="text/javascript" src="../flot/curvedLines.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.orderBars.js"></script>
		<link href="style.css" rel="stylesheet" type="text/css">
 	</head>
	<body>
		<div id="CompleteContainer">
			<div id="Header">
				<div id="NAOImage"><img src="../nao.png"/></div>
				<div id="HeaderTitle"><h1>Robotic attack by NAO</h1></div>
				<div><hr/></div>
			</div>
			<div id="DemoOptions">
			<!--
				<span id="UserSelectionSpan">Select user being attacked:
						<select id="SelectUserMenu" class="UserSelection">
							<option <?=($CurrentUser=="Suzzett")?"selected":""?> value="Suzzett">Suzzett</option>
							<option <?=($CurrentUser=="User2")?"selected":""?> value="User2">User2</option>
							<option <?=($CurrentUser=="User3")?"selected":""?> value="User3">User3</option>
							<option <?=($CurrentUser=="User4")?"selected":""?> value="User4">User4</option>
							<option <?=($CurrentUser=="User5")?"selected":""?> value="User5">User5</option>
						</select>
					</span>
			-->
				<span id="InitializeButtonSpan"><input type="button" id="StartData" class="CustomButton" value="Initialize"/></span>				
			</div>
			<div class="Clear"><hr/></div>
	 		<div id="PlotsContainer">
	 			<table id="PlotsTable">
	 				<tr class="GeneralTR">
	 					<td class="GeneralTD">
							<div class="GraphLabel">X Axis</div><div id="XAxisPlot"></div>	
	 					</td>
	 					<td class="GeneralTD">
							<div class="GraphLabel">Y Axis</div><div id="YAxisPlot"></div>					
	 					</td>
	 				</tr>
	 				<tr class="GeneralTR">
	 					<td class="GeneralTD">
							<div class="GraphLabel">Area</div><div id="AreaPlot"></div>
	 					</td>
	 					<td class="GeneralTD">
							<div class="GraphLabel">Pressure</div><div id="PressurePlot"></div>
	 					</td>
	 				</tr>
	 				<tr class="ClassifierTR">
	 					<td colspan=2 class="ClassifierTD">
							<div class="">Similarity in features</div><div id="ClassifierScorePlot"></div>
	 					</td>
	 				</tr>
	 			</table>
	 			<!--<ul id="PlotsUL">
	 				<!--<li><div class="GraphLabel">X-Y Screen</div><div id="AxisPlot"></div></li>
	 				<li><div class="GraphLabel">X Axis</div><div id="XAxisPlot"></div></li>
	 				<li><div class="GraphLabel">Y Axis</div><div id="YAxisPlot"></div></li>
	 				<li><div class="GraphLabel">Area</div><div id="AreaPlot"></div></li>
	 				<li><div class="GraphLabel">Pressure</div><div id="PressurePlot"></div></li>
	 				<li class="ClassifierLi"><div class="GraphLabel">Classifier Score</div><div id="ClassifierScorePlot"></div></li>
	 			</ul>-->
	 		</div>
			<div class="Clear"><hr/></div>
		</div>
		<script src="Initialization.js"></script>
		<script>
			//SelectedUser = "<?=$CurrentUser?>";
			AllUserNames = "NAOSwipe,"+SelectedUser;
			AllUserNames = AllUserNames.replace(/\s/g, '');
			console.log("New all user names = "+AllUserNames);
			Users = AllUserNames.split(",");
			$(function()
			{
				$("#updateInterval").val(updateInterval).change(function () 
				{
					var v = $(this).val();
					if (v && !isNaN(+v)) {
						updateInterval = +v;
						if (updateInterval < 1) {
							updateInterval = 1;
						} else if (updateInterval > 2000) {
							updateInterval = 2000;
						}
						$(this).val("" + updateInterval);
					}
				});
				
					var ClassifierUpdateInterval = 1000;
					$("#ClassifierUpdateInterval").val(ClassifierUpdateInterval).change(function () 
					{
						var v = $(this).val();
						if (v && !isNaN(+v)) {
							ClassifierUpdateInterval = +v;
							if (ClassifierUpdateInterval < 1) {
								ClassifierUpdateInterval = 1;
							} else if (ClassifierUpdateInterval > 2000) {
								ClassifierUpdateInterval = 2000;
							}
							$(this).val("" + ClassifierUpdateInterval);
						}
					});					

				$("#SecondaryInterval").val(SecondaryInterval).change(function () 
				{
					var v = $(this).val();
					if (v && !isNaN(+v)) {
						SecondaryInterval = +v;
						if (SecondaryInterval < 1) {
							SecondaryInterval = 1;
						} else if (SecondaryInterval > 2000) {
							SecondaryInterval = 2000;
						}
						$(this).val("" + SecondaryInterval);
					}
				});
				$.getScript('AxisPlot.js', function()				{console.log("Axis Plot Loaded")});
				$.getScript('PressurePlot.js', function()		{console.log("Pressure Plot Loaded")});
				$.getScript('AreaPlot.js', function()				{console.log("Area Plot Loaded")});
				$.getScript('FeatureSimilarity.js', function()	{console.log("Feature Similarity Plot Loaded")});
				/******************************************************************************/
				//Classifier score data graph
				/******************************************************************************/

					function GetClassifierScoreData()
					{
						if (ClassifierScoreGData.length > 0)
							ClassifierScoreGData = ClassifierScoreGData.slice(1);

						// Do a random walk

						while (ClassifierScoreGData.length < ClassifierScoreTotalPoints)
						{
							if (ClassifierScoreData.length > ClassifierScorePlotCounter)
							{
								ClassifierScoreGData.push(ClassifierScoreData[ClassifierScorePlotCounter]);
								ClassifierScorePlotCounter++;
							}
							else
								ClassifierScoreGData.push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < ClassifierScoreGData.length; ++i) {
							if (ClassifierScoreGData[i]!=0) console.log("Adding val = "+ClassifierScoreGData[i]);
								res.push([i, ClassifierScoreGData[i]])
						}
						return res;
						
					}

					/******************************************************************************/				
					ClassifierSeriesObj = function() 
					{
						var RobotData;
						var UserData;
						if (UserPlotPoints.length > 0)
						{
							RobotData = RobotPlotPoints.shift();
							UserData = UserPlotPoints.shift();
						}
							
						 return [
							{
								data: RobotData,
                color: 'red',
                bars: {
                    show: true,
                    barWidth: 0.3,
                    order: 2
                },
							}, 
							{
								data: UserData,
                color: 'black',
                bars: {
                    show: true,
                    barWidth: 0.3,
                    order: 2
                },
							},
						];
					}
					var ClassifierOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						yaxis: {
							min: 0,
							max: 1
						},
						xaxis: {
							ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time'],[5,'Angle']],
							min: -1,
							max: 6
						},
						grid: 
						{
            	aboveData: true        		}				
			
					}

					/******************************************************************************/
					var ClassifierPlot = $.plot("#ClassifierScorePlot", ClassifierSeriesObj(), ClassifierOptions);
					/******************************************************************************/
					function UpdateClassifierPlot() 
					{
						ClassifierPlot.setData(ClassifierSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						ClassifierPlot.draw();
						setTimeout(UpdateClassifierPlot, ClassifierUpdateInterval);
					}
					UpdateClassifierPlot();			});
		</script>
	</body>
</html>
