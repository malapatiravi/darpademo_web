<?
	$TrainUsers = ["User1", "User2", "User3", "User4", "User5"];
	$TestUsers = ["User1", "User2", "User3", "User4", "User5"];
	$CurrentTrainUser = (isset($_GET['TrUser'])?$_GET['TrUser']:$TestUsers[0]);
	$CurrentTestUser = (isset($_GET['TeUser'])?$_GET['TeUser']:$TestUsers[1]);
?>
<!DOCTYPE html>
<html>
 	<head>
 		<title>Active Authentication Based On Body Movement Patterns</title>
 		<script type="text/javascript" src="../jquery-1.11.0.min.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.js"></script>
		<script language="JavaScript" type="text/javascript" src="../flot/curvedLines.js"></script>
		<script language="javascript" type="text/javascript" src="../flot/jquery.flot.orderBars.js"></script>
		<link href="style3.css" rel="stylesheet" type="text/css">
 	</head>
	<body>
		<div id="CompleteContainer">
			<div id="Header"><h1>Swiping features match</h1></div>
			<div class="Clear"><hr/></div>

			<div class="Clear"><br/></div>
			<div id="InitializeButtonSpan"><input type="button" id="StartData" class="StartBtn" value="Initiate"/></div>
			<div class="Clear"><hr/></div>
			<div id="showdata"></div>
			<div id="ScoreVal"></div>
			<div class="Clear"></div>
			<div class="">Similarity in features</div><div id="ClassifierScorePlot"></div>
			<script>
				var LoopInterval = 1000;
				var BackgroundColor = "#000000";
				
				var UserInitX = new Array();
				var UserInitY = new Array();
				var UserEndX = new Array();
				var UserEndY = new Array();
				var UserDistance = new Array();
				var UserTime = new Array();

				var RobotInitX = new Array();
				var RobotInitY = new Array();
				var RobotEndX = new Array();
				var RobotEndY = new Array();
				var RobotDistance = new Array();
				var RobotTime = new Array();
				
				var RobotPlotPoints = new Array();
				var UserPlotPoints = new Array();

				var TotalPoints = 0;
				var IntLineNumbers = new Array();
				var TotalForEach = new Array();
				var ReadStatus = "1,1";
				var TotalUsers = 2;
				var DemoFolder = "DEMO/sensors/Datum";
				var Mode = "Training";
				var Pause = false;
				var Counter = 0;
				var RunAjax;
				var XAxisData = new Array();
				var YAxisData = new Array();
				var ZAxisData = new Array();
				var MData = new Array();
				var ClassifierData = new Array();
				var SwipeTypeData = new Array();
				var Users = "User1,User2";
				var	StringLineNumber = "1,1";
				//var Initialized = false;

				var ScoreFilename = "";
				var ScoreFolder = DemoFolder+"/Scores";
				for (i=0; i<TotalUsers;i++)
				{
					TotalForEach[i] = 0;
					IntLineNumbers[i] = 0;
					XAxisData[i] = new Array();
					YAxisData[i] = new Array();
					ZAxisData[i] = new Array();
					MData[i] = new Array();
					ClassifierData[i] = new Array();
					SwipeTypeData[i] = new Array();
					ScoreFilename += ((i==0)?"":"-")+Users[i];
				}
				ScoreFilename+=".txt";
				console.log("Scorefile name = "+ScoreFilename+" in "+ScoreFolder);
				$(document).ready(function()
				{
						$('#StartData').on('click', function()
						{
              RunAjax = setInterval("GetDataFromServer()", LoopInterval);
              //Initialized = true;
						});					
				});
				function GetDataFromServer()
				{
					if (!Pause)
					{
						$.ajax({
							type: 'POST',
							url: "GetBothData.php",
							data: {LineNumbers: StringLineNumber, Statuses: ReadStatus},
							success: function(ServerData)
							{
									console.log("Linenumber: "+StringLineNumber+", Statuses="+ReadStatus+", Data = "+ServerData.Data[1]);
									var GetStatuses = ServerData.Statuses.split(",");
									TotalPoints++;
									for (i=0; i<TotalUsers;i++)
									{
										var SplittedData = ServerData.Data[i].split(",");
										if (GetStatuses[i]!="0")
										{
											$('#showdata').html("<p>Line number="+ServerData.LineNumber+" Data="+ServerData.Data[1]+", Status1="+GetStatuses[0]+", Status2="+GetStatuses[1]+"</p>");
											IntLineNumbers[i] = IntLineNumbers[0]+1;
											if (i==0)
											{
												RobotInitX[TotalPoints] = SplittedData[0]/1080;											
												RobotInitY[TotalPoints] = SplittedData[1]/1920;											
												RobotEndX[TotalPoints] = SplittedData[2]/1080;											
												RobotEndY[TotalPoints] = SplittedData[3]/1920;											
												RobotDistance[TotalPoints] = SplittedData[4]/2203;											
												RobotTime[TotalPoints] = SplittedData[5]/1000;
												var TempPlotPoints = [[0,RobotInitX[TotalPoints]],[1,RobotInitY[TotalPoints]],[2,RobotEndX[TotalPoints]],[3,RobotEndY[TotalPoints]],[4,RobotDistance[TotalPoints]],[5,RobotTime[TotalPoints]]];
												RobotPlotPoints.push(TempPlotPoints);
												//console.log((TempPlotPoints).toString());
											}
											else
											{
												UserInitX[TotalPoints] = SplittedData[0]/1080;									
												UserInitY[TotalPoints] = SplittedData[1]/1920;										
												UserEndX[TotalPoints] = SplittedData[2]/1080;											
												UserEndY[TotalPoints] = SplittedData[3]/1920;										
												UserDistance[TotalPoints] = SplittedData[4]/2203;											
												UserTime[TotalPoints] = SplittedData[5]/1000;
												var TempPlotPoints = [[0,UserInitX[TotalPoints]],[1,UserInitY[TotalPoints]],[2,UserEndX[TotalPoints]],[3,UserEndY[TotalPoints]],[4,UserDistance[TotalPoints]],[5,UserTime[TotalPoints]]];
												UserPlotPoints.push(TempPlotPoints);
												//console.log(RobotTime);
											}
										}
										//console.log("Total for "+i+"="+TotalForEach[i]);
									}
									StringLineNumber = IntLineNumbers[0]+","+IntLineNumbers[1];
							},
							error : function(jqXHR, textStatus, errorThrown)
							{
								console.log("There was an error: "+errorThrown+" variables: StringLineNumber="+StringLineNumber+", AllUserNames="+AllUserNames+", Mode="+Mode+", Folder="+DemoFolder+", Status="+ReadStatus);
							},
							dataType: "json"
						});
					}
				}
				var CurrentScoreLine = 0;
				var ScoreFileStatus = 1;
				var ScoreVal = 50;
				var PrevScoreVal = 50;
				
				

				var XAxisPlotCounter = 0;
				var YAxisPlotCounter = 0;
				var ZAxisPlotCounter = 0;
				var MPlotCounter = 0;
				var ClassifierPlotCounter = 0;

				$(function()
				{
					var XAxisGData = new Array(), 		XAxisTotalPoints = 200;
					var YAxisGData = new Array(), 		YAxisTotalPoints = 200;
					var ZAxisGData = new Array(), 	ZAxisTotalPoints = 200;
					var MGData = new Array(), 			MTotalPoints = 200;
					var ClassifierGData = new Array(), 			ClassifierTotalPoints = 6;
					var updateInterval = 50;
					$("#updateInterval").val(updateInterval).change(function () 
					{
						var v = $(this).val();
						if (v && !isNaN(+v)) {
							updateInterval = +v;
							if (updateInterval < 1) {
								updateInterval = 1;
							} else if (updateInterval > 2000) {
								updateInterval = 2000;
							}
							$(this).val("" + updateInterval);
						}
					});
					var ClassifierUpdateInterval = 1000;
					$("#ClassifierUpdateInterval").val(ClassifierUpdateInterval).change(function () 
					{
						var v = $(this).val();
						if (v && !isNaN(+v)) {
							ClassifierUpdateInterval = +v;
							if (ClassifierUpdateInterval < 1) {
								ClassifierUpdateInterval = 1;
							} else if (ClassifierUpdateInterval > 2000) {
								ClassifierUpdateInterval = 2000;
							}
							$(this).val("" + ClassifierUpdateInterval);
						}
					});					
					
					for (i=0; i<TotalUsers;i++)
					{
						XAxisGData[i] = new Array();
						YAxisGData[i] = new Array();
						ZAxisGData[i] = new Array();
						MGData[i] = new Array();
						ClassifierGData[i] = new Array();
					}

				/******************************************************************************/
				//Classifier data graph
				/******************************************************************************/

					function GetClassifierData(UserNumber)
					{
						if (ClassifierGData[UserNumber].length > 0)
							ClassifierGData[UserNumber] = ClassifierGData[UserNumber].slice(1);

						// Do a random walk

						while (ClassifierGData[UserNumber].length < ClassifierTotalPoints)
						{
							if (ClassifierData[UserNumber].length > ClassifierPlotCounter)
							{
									ClassifierGData[UserNumber].push(ClassifierData[UserNumber][ClassifierPlotCounter]);
									//console.log(XAxisData[XAxisPlotCounter]);
									ClassifierPlotCounter++;
							}
							else
								ClassifierGData[UserNumber].push(0);
						}
						// Zip the generated y values with the x values
						var res = [];
						for (var i = 0; i < ClassifierGData[UserNumber].length; ++i) {
							if (UserNumber==1)
								res.push([i, ClassifierGData[UserNumber][i]]);
							else
								res.push([i, 0.2]);							
						}
						return res;
						
					}

					/******************************************************************************/				
					ClassifierSeriesObj = function() 
					{
						var RobotData;
						var UserData;
						if (UserPlotPoints.length > 0)
						{
							RobotData = RobotPlotPoints.shift();
							UserData = UserPlotPoints.shift();
						}
							
						 return [
							{
								data: RobotData,
                bars: {
                    show: true,
                    barWidth: 0.03,
                    order: 2
                },
							}, 
							{
								data: UserData,
                bars: {
                    show: true,
                    barWidth: 0.03,
                    order: 2
                },
							},
						];
					}
					var ClassifierOptions = 
					{
						series: {
							shadowSize: 0	// Drawing is faster without shadows
						},
						yaxis: {
							min: 0,
							max: 1
						},
						xaxis: {
							ticks:[[0,'Initial X'],[1,'Initial Y'],[2,'Final X'],[3,'Final Y'],[4,'Time'],[5,'Angle']],
							min: -1,
							max: 6
						},
						grid: 
						{
            	aboveData: true,
            	backgroundColor: BackgroundColor,
        		}				
			
					}

					/******************************************************************************/
					var ClassifierPlot = $.plot("#ClassifierScorePlot", ClassifierSeriesObj(), ClassifierOptions);
					/******************************************************************************/
					function UpdateClassifierPlot() 
					{
						ClassifierPlot.setData(ClassifierSeriesObj());
						// Since the axes don't change, we don't need to call plot.setupGrid()
						ClassifierPlot.draw();
						setTimeout(UpdateClassifierPlot, ClassifierUpdateInterval);
					}
					UpdateClassifierPlot();
					/******************************************************************************/
					/******************************************************************************/
					$(".flot-text").css('color','#22ff22 !important');
				});	
				
			</script>
		</div>
		
	</body>
</html> 
